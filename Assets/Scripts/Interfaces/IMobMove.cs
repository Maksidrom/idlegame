using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMobMove
{
    void MoveToPlayer();
}
