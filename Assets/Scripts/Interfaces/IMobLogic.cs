using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMobLogic
{
    void SetMobParameteres(int attackPower, int attackSpeed, int attackLength);
    void Attack(GameObject targetGameObject);
}
