using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerHP
{
    void GetDamage(int damage);
    void LevelUp(int newLevel);
    void Heal(int heal);
    void Death();
}
