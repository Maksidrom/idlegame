using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMobHP
{
    bool GetDamage(int damage);
    void GetHeal(int heal);
    void Death();
}
