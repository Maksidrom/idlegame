using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class Abilities : MonoBehaviour, IAbilities
{
    public Transform SplashAnimation;

    private float _powerAttackCooldown;

    private float _splashAttackCooldown;

    private float _incraseMaxHpCooldown;
    // Start is called before the first frame update
    void Start()
    {
        _powerAttackCooldown = 5f;
        _splashAttackCooldown = 5f;
        _incraseMaxHpCooldown = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int PowerAttack(int currentDamage, bool increase)
    {
        //Increases damage by n times
        if (increase)
        {
            currentDamage = currentDamage * 2;
            return currentDamage;    
        }

        return currentDamage / 2;
    }

    public void SplashAttack(int currentDamage, Vector3 position)
    {
        float timeToDestroy = 0.8f;
        var instantiate = Instantiate(SplashAnimation, new Vector3(-8.7f, -1.7f, -0.8f), Quaternion.identity);
        instantiate.GetComponent<SplashAttack>().GetSpellDamageAndTarget(currentDamage);
        SkillBarController.Instance.SecondSpellCooldownEffect(_splashAttackCooldown);
        Destroy(instantiate.gameObject,timeToDestroy);
    }

    public void IncraseMaxHP()
    {
        var maxHp = transform.GetComponent<PlayerHP>();
        int hpAbilitiStart = maxHp.MaxHp;
        maxHp.IncreasedMaxHPAbilitiy(true, 20);
        SkillBarController.Instance.ThirdSpellCooldownEffect(_incraseMaxHpCooldown);
        StartCoroutine(ReturnHp(hpAbilitiStart));
    }

    IEnumerator ReturnHp(int hp)
    {
        yield return new WaitForSeconds(3);
        transform.GetComponent<PlayerHP>().IncreasedMaxHPAbilitiy(false, hp);
    }
}
