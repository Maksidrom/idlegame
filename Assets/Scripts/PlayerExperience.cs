using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerExperience : MonoBehaviour
{
    [SerializeField] private Text PlayerExperienceText;
    [SerializeField] private Slider expSlider;
    [SerializeField] private Text PlayerLevelText;

    private float _experience;
    private float _experienceForNewLevel = 100f;
    private int _level;
    private PlayerHP playerHp;

    // Start is called before the first frame update
    void Start()
    {
        expSlider.value = 0;
        _level = 1;
        playerHp = GetComponent<PlayerHP>();
        UpdatePlayerLevelEndExperience();
    }
    
    public float Experience
    {
        get { return _experience; }
        set
        {            
            _experience = _experience + value;
            if (_experience >= 100 * _level)
            {
                _level++;
                _experience = 0;
                playerHp.LevelUp(_level);
            }
            UpdatePlayerLevelEndExperience();
        }
    }
    
    private void UpdatePlayerLevelEndExperience()
    {
        PlayerExperienceText.text = "Level: " + _level;
        expSlider.value = _experience / (_experienceForNewLevel * _level);
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Experience = 20;
            UpdatePlayerLevelEndExperience();
        }
    }
}
