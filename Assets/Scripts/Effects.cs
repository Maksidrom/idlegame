using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Effects : MonoBehaviour
{
    public static Effects Instance;

    [SerializeField] private ParticleSystem cloudsEffect;
    [SerializeField] private ParticleSystem powerAttackEffect;
    private ParticleSystem newPowerAttackParticleSystem;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Несколько экземпляров Effects");
        }

        Instance = this;
    }

    public void CreatePowerAttackParticle()
    {
        //instantiate(powerAttackEffect, new Vector3(-8.78f,-1.9f,0));
        newPowerAttackParticleSystem = Instantiate(powerAttackEffect, new Vector3(-8.78f, -1.9f, 0), Quaternion.identity);
    }

    public void DestroyPowerAttackParticle()
    {
        try
        {
            Destroy(newPowerAttackParticleSystem);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    public void Explosion(Vector3 position)
    {
        instantiate(cloudsEffect, position);
    }

    private ParticleSystem instantiate(ParticleSystem prefab, Vector3 position)
    {
        ParticleSystem newParticleSystem = Instantiate(prefab,position,Quaternion.identity) as ParticleSystem;
        
        Destroy(newParticleSystem.gameObject, newParticleSystem.startLifetime);

        return newParticleSystem;
    }
}
