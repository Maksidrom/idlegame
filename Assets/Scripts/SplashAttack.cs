using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashAttack : MonoBehaviour, IAttack
{
    private int damage;

    public void GetSpellDamageAndTarget(int damage)
    {
        this.damage = damage * 2;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Enemy"))
        {
            Attack(other.transform, damage);
        }
    }

    public void Attack(Transform target, int damage)
    {
        target.GetComponent<MobHP>().GetDamage(damage);
    }
}
