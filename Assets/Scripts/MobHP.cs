using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobHP : MonoBehaviour, IMobHP
{
    [SerializeField] private int HP;
    [SerializeField] private GameObject textDamage;
    [SerializeField] private GameObject playerGameObject;
    public Animator Animator;

    private TextMesh _textMesh;
    // Start is called before the first frame update
    void Start()
    {
        playerGameObject = GameObject.FindWithTag("Player");
        HP = 100;
        _textMesh = textDamage.GetComponent<TextMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool GetDamage(int damage)
    {
        HP -= damage;
        StartCoroutine(ShowDamageTaken(damage.ToString()));
        
        if (HP <= 0)
        {
            Death();
            return true;
        }

        return false;
    }

    IEnumerator ShowDamageTaken(string damage)
    {
        _textMesh.text = damage;
        yield return new WaitForSeconds(0.5f);
        _textMesh.text = "";

    }

    IEnumerator DeathParticlEnumerator()
    {
        yield return new WaitForSeconds(0.8f);
        Effects.Instance.Explosion(transform.position);
        transform.GetComponent<MobMove>().SayAnotherMobWeDie();
        Destroy(gameObject);
    }

    public void GetHeal(int heal)
    {
        throw new System.NotImplementedException();
    }

    public void Death()
    {
        StartCoroutine(DeathParticlEnumerator());
        transform.GetComponent<Mob>().GetPlayerExperience();
        Destroy(transform.GetComponent<Rigidbody>());
        
        
        if (transform.name == "Goblin(Clone)")
        {
            Animator.Play("Die");
        }
    }
}
