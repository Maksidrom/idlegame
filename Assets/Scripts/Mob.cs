using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob : MonoBehaviour, IMobLogic
{
    [SerializeField] private float attackLength; 
    [SerializeField] private float attackSpeed;
    [SerializeField] private GameObject playerGameObject;
    [SerializeField] private int attackPower;
    private Transform thisTransform;
    private MobMove moveMob;
    private bool mobAlive;


    // Start is called before the first frame update
    void Start()
    {
        playerGameObject = GameObject.FindWithTag("Player");
        thisTransform = transform;
        moveMob = transform.GetComponent<MobMove>();
        attackLength = 2f;
        attackSpeed = 1;
        attackPower = 10;
        mobAlive = true;
    }

    public bool MobAlive => mobAlive;

    public float AttackLength => attackLength;

    public void SetMobParameteres(int attackPower, int attackSpeed, int attackLength)
    {
        this.attackPower = attackPower;
        this.attackSpeed = attackSpeed;
        this.attackLength = attackLength;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerGameObject != null && mobAlive)
        {
            if (Vector3.Distance(transform.position, playerGameObject.transform.position) < attackLength)
            {
                moveMob.PathIsClear = false;
                if (attackSpeed >= 1)
                {
                    Attack(playerGameObject);
                    attackSpeed = 0;
                }
            }
        }

        if (attackSpeed < 1)
        {
            attackSpeed += Time.deltaTime;
        }
    }

    public void GetPlayerExperience()
    {
        playerGameObject.GetComponent<PlayerExperience>().Experience = 20;
        playerGameObject.GetComponent<PlayerLogic>().RemoveTargetFromArray(transform);
        mobAlive = false;
    }

    public void Attack(GameObject targetGameObject)
    {
        var playerHp = targetGameObject.transform.GetComponent<PlayerHP>();
        playerHp.GetDamage(attackPower);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Player"))
        {
            playerGameObject = other.gameObject;
        }
    }
}
