using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobAttack : MonoBehaviour, IAttack
{
    private Transform target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Attack(Transform target, int damage)
    {
        if (target.CompareTag("Player"))
        {
            target.GetComponent<PlayerHP>().GetDamage(damage);
        }
    }
}
