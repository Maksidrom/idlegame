using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobMove : MonoBehaviour, IMobMove
{
    [SerializeField] private GameObject playerGameObject;
    private float mobAttackRange;
    private Transform thisTransform;
    private GameObject gameobjectOtherEnemy;

    private bool pathIsClear;
    // Start is called before the first frame update
    void Start()
    {
        thisTransform = transform;
        pathIsClear = true;
        playerGameObject = GameObject.FindWithTag("Player");
        mobAttackRange = transform.GetComponent<Mob>().AttackLength;
    }

    public bool PathIsClear
    {
        get { return pathIsClear; }
        set { pathIsClear = value; }
    }

    // Update is called once per frame
    void Update()
    {
        if (pathIsClear)
        {
            MoveToPlayer();
        }
    }

    public void SayAnotherMobWeDie()
    {
        if (gameobjectOtherEnemy != null)
        {
            gameobjectOtherEnemy.GetComponent<MobMove>().PathIsClear = true;     
        }
    }

    public void MoveToPlayer()
    {
        if (gameobjectOtherEnemy != null)
        {
            gameobjectOtherEnemy.GetComponent<MobMove>().PathIsClear = true;
        }

        if (playerGameObject != null)
        {
            if (Vector3.Distance(transform.position, playerGameObject.transform.position) > mobAttackRange)
            {
                transform.position += new Vector3(-1, 0, 0) * Time.deltaTime;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.transform.name);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Enemy"))
        {
            pathIsClear = false;
        }

        if (transform.position.x < other.transform.position.x)
        {
            gameobjectOtherEnemy = other.gameObject;
            Debug.Log("Say: " + transform.name + " detect back: " + other.transform.name);    
        }
    }
}
