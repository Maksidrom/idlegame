using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillBarController : MonoBehaviour
{
    public static SkillBarController Instance;
    [SerializeField] private Image firstSpellImageCooldown;
    [SerializeField] private Image secondSpellImageCooldown;
    [SerializeField] private Image thirdSpellImageCooldown;
    
    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Несколько экземпляров SkillBarController");
        }

        Instance = this;
    }

    public void FristSpellCooldownEffect(float timeCooldown)
    {
        StartCoroutine(FirstSpellBarCooldown(timeCooldown));
    }

    public void SecondSpellCooldownEffect(float timeCooldown)
    {
        StartCoroutine(SecondSpellBarCooldown(timeCooldown));
    }

    public void ThirdSpellCooldownEffect(float timeCooldown)
    {
        StartCoroutine(ThirdSpellBarCooldown(timeCooldown));
    }


    private IEnumerator FirstSpellBarCooldown(float timeCooldown)
    {
        while (firstSpellImageCooldown.fillAmount < 1)
        {       
            yield return null;
            firstSpellImageCooldown.fillAmount += 1f / timeCooldown * Time.deltaTime;
        }
        
        firstSpellImageCooldown.fillAmount = 0;        
    }
    
    IEnumerator SecondSpellBarCooldown(float timeCooldown)
    {
        while (secondSpellImageCooldown.fillAmount < 1)
        {
            yield return null;
            secondSpellImageCooldown.fillAmount += 1f / timeCooldown * Time.deltaTime;
        }

        secondSpellImageCooldown.fillAmount = 0;        
    }
    
    IEnumerator ThirdSpellBarCooldown(float timeCooldown)
    {
        while (thirdSpellImageCooldown.fillAmount < 1)
        {
            yield return null;
            thirdSpellImageCooldown.fillAmount += 1f / timeCooldown * Time.deltaTime;
        }

        thirdSpellImageCooldown.fillAmount = 0;        
    }
}
