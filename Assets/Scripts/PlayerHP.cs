using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHP : MonoBehaviour, IPlayerHP
{
    [SerializeField] private int HP;
    [SerializeField] private int MaxHP;
    [SerializeField] private GameObject eventGameObject;

    private int currentLvl;
    private int SavedMaxHP;
    private int savedLvl;
    
    public Text PlayerHpText;
    public GameObject textDamage;
    private TextMesh _textMeshShowDamage;
    private Text showHpAndMaxHP;
    
    private bool hpIncreaseByAbilities;

    // Start is called before the first frame update
    void Start()
    {
        MaxHP = 300;
        HP = MaxHP;
        ShowHpAndMaxHp();
        _textMeshShowDamage = textDamage.GetComponent<TextMesh>();
    }

    public int MaxHp
    {
        get { return MaxHP; }
        set { MaxHP = value;
            if (HP > value)
            {
                HP = value;
            }
            ShowHpAndMaxHp();
        }
    }

    public int CurrentHp
    {
        get { return HP; }
        set
        {
            HP = value;
            ShowHpAndMaxHp();
        }
    }

    public void GetDamage(int damage)
    {
        HP -= damage;
        StartCoroutine(ShowDamageTaken(damage.ToString()));
        ShowHpAndMaxHp();
        if (HP <= 0)
        {
            Destroy(gameObject);
            Death();
        }
    }

    private void ShowHpAndMaxHp()
    {
        PlayerHpText.text = HP.ToString() + " / " + MaxHP.ToString();
    }
    
    public void IncreasedMaxHPAbilitiy(bool incraseHP, int multiplier)
    {
        if (incraseHP)
        {
            SavedMaxHP = MaxHP;
            savedLvl = currentLvl;
            MaxHP = MaxHp + MaxHP *  multiplier / 100;
            CurrentHp = CurrentHp + CurrentHp * multiplier / 100;
            hpIncreaseByAbilities = incraseHP;
            hpIncreaseByAbilities = true;
        }
        else
        {
            hpIncreaseByAbilities = false;
            if (savedLvl != currentLvl)
            {
                MaxHP = SavedMaxHP + SavedMaxHP * 10 / 100;
                if (CurrentHp > MaxHP)
                {
                    CurrentHp = MaxHP;
                }

                ShowHpAndMaxHp();
                return;
            }
            MaxHp = SavedMaxHP;
            if (CurrentHp > MaxHP)
            {
                CurrentHp = MaxHP;
            }
            ShowHpAndMaxHp();
        }
    }

    public void LevelUp(int newLevel)
    {
        currentLvl = newLevel;
        MaxHP += MaxHp * 10 / 100;
        CurrentHp = MaxHP;
    }

    public void Heal(int heal)
    {
        throw new System.NotImplementedException();
    }

    public void Death()
    {
        var sendWeDead = eventGameObject.GetComponent<Events>();
        sendWeDead.DisplayScore();
    }

    IEnumerator ShowDamageTaken(string damage)
    {
        _textMeshShowDamage.text = damage;
        yield return new WaitForSeconds(0.5f);
        _textMeshShowDamage.text = "";
    }
}
