using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAbilities
{
    int PowerAttack(int currentDamage, bool increase);
    void SplashAttack(int currentDamage, Vector3 position);
    void IncraseMaxHP();
}
