using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobSpawn : MonoBehaviour
{
    [SerializeField] private Transform goblinTransform;
    [SerializeField] private Transform entTransform;
    [SerializeField] private int level;
    [SerializeField] private float spawnTimeMob = 1;
    [SerializeField] private float spawnCooldownMob;
    private int count;
    
    // Start is called before the first frame update
    void Start()
    {
        level = 1;
        count = 0;
        spawnCooldownMob = spawnTimeMob;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            SpawnGoblin();
        }

        if (spawnCooldownMob <= 0)
        {
            if (count == 5)
            {
                SpawnEnt();
                count = 0;
            }
            else
            {
                SpawnGoblin();
            }
            spawnCooldownMob = spawnTimeMob;
            count++;
        }

        if (spawnCooldownMob > 0)
        {
            spawnCooldownMob -= Time.deltaTime;
        }
    }

    private void SpawnEnt()
    {
        var newEnt = Instantiate(entTransform, new Vector3(0, -0.93f, 0), Quaternion.identity);
        newEnt.GetComponent<Mob>().SetMobParameteres(30 * level, 1, 2);
    }

    private void SpawnGoblin()
    {
        var newGoblin = Instantiate(goblinTransform, new Vector3(0, -1.52f, 0), Quaternion.identity);
        newGoblin.GetComponent<Mob>().SetMobParameteres(10 * level,1,2);
    }
}
