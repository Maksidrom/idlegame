using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLogic : MonoBehaviour, IAttack
{
    [SerializeField] private int attackDamage;
    [SerializeField] private float attackSpead;
    [SerializeField] private float attackCooldown;
    [SerializeField] private List<Transform> enemys;
    [SerializeField] private bool firstSpellActive;
    [SerializeField] private bool secondSpellActive;
    [SerializeField] private bool thirdSpellActive;
    [SerializeField] private Animator Animator;
    
    private Transform enemy;
    private float firstSpellCooldown = 0;
    private float secondSpellCooldown = 0;
    private float thirdSpellCooldown = 0;
    
    private PlayerHP _playerHp;
    private Abilities _abilities;
    
    // Start is called before the first frame update
    void Start()
    {
        attackDamage = 60;
        attackSpead = 1;
        enemys = new List<Transform>();
        _playerHp = GetComponent<PlayerHP>();
        _abilities = transform.GetComponent<Abilities>();
    }

    

    // Update is called once per frame
    void Update()
    {
        if (enemy != null && attackCooldown <= 0)
        {
            Attack(enemy, attackDamage);
            attackCooldown = attackSpead;
        }
        else if (enemy == null)
        {
            if (enemys.Count > 0)
            {
                for (int i = 0; i < enemys.Count; i++)
                {
                    if (enemys[i] == null)
                    {
                        RemoveTargetFromArray(enemys[i]);
                    }
                    else
                    {
                        enemy = enemys[i];
                        return;
                    }
                }
            }
        }

        if (attackCooldown > 0)
        {
            attackCooldown -= Time.deltaTime;
        }

        if (firstSpellCooldown > 0)
        {
            firstSpellCooldown -= Time.deltaTime;
            Effects.Instance.DestroyPowerAttackParticle();
        }
        
        if (secondSpellCooldown > 0)
        {
            secondSpellCooldown -= Time.deltaTime;
        }
        
        if (thirdSpellCooldown > 0)
        {
            thirdSpellCooldown -= Time.deltaTime;
        }
    }

    public void FirstSpellActivate()
    {
        if (firstSpellCooldown <= 0 && firstSpellActive == false)
        {
            firstSpellActive = true;
            Effects.Instance.CreatePowerAttackParticle();
        }

    }

    public void SecondSpellActivate()
    {
        //secondSpellActive = true;
        if (secondSpellCooldown <= 0)
        {
            _abilities.SplashAttack(attackDamage, transform.position);
            secondSpellCooldown = 5;
        }
    }

    public void ThirdSpellActivate()
    {
        if (thirdSpellCooldown <= 0)
        {
            _abilities.IncraseMaxHP();
            thirdSpellCooldown = 5;
        }
    }

    private int FirstSpell(int attack, bool incraseDamage)
    {
        return _abilities.PowerAttack(attack, incraseDamage);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Enemy") && other.transform.GetComponent<Mob>().MobAlive)
        {
            enemys.Add(other.transform);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Enemy") && other.GetComponent<Mob>().MobAlive)
        {
            enemys.Add(other.transform);
        }
    }

    public void RemoveTargetFromArray(Transform targeTransform)
    {
        enemys.Remove(targeTransform);
    }

    private void StateMachine(int state)
    {
        switch (state)
        {
                case 1:
                    Animator.Play("CharacterIdle");
                    break;
                
                case 2:
                    Animator.Play("Attack");
                    break;
        }
    }

    public void Attack(Transform target, int damage)
    {
        var targetHp = target.transform.GetComponent<MobHP>();
        if (firstSpellActive)
        {
            if (targetHp.GetDamage(FirstSpell(damage, true)))
            {
                RemoveTargetFromArray(target);
                SkillBarController.Instance.FristSpellCooldownEffect(5);
                firstSpellCooldown = 5;
                firstSpellActive = false;
            }  
        }
        else
        {
            if (targetHp.GetDamage(damage))
            {
                RemoveTargetFromArray(target);
            }
            StateMachine(2);
        }
    }
}
